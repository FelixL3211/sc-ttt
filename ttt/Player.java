package ttt;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;


import ttt.Gameboard.state;

public class Player {
    public state piece;
    public int nrOfPieces;
    public DataOutputStream out;
    public DataInputStream in;

    public Player(state s, DataOutputStream output, DataInputStream input ){
        piece = s;
        nrOfPieces = 0;
        out = output;
        in = input;
    }
}
