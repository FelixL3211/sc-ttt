package ttt;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import ttt.Gameboard.state;


public class Server {
    private ServerSocket serverSocket;
    private Socket clientSocket1;
    private Socket clientSocket2;
    private DataOutputStream out1;
    private DataInputStream in1;
    private DataOutputStream out2;
    private DataInputStream in2;
    private Gameboard gb;


    public void gameloop(Player p1, Player p2) throws IOException{
        gb = new Gameboard();
        p1.out.writeByte(Constants.YOUSTART);
        p2.out.writeByte(Constants.OPPSTART);
        
    }

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientSocket1 = serverSocket.accept();
        out1 = new DataOutputStream(clientSocket1.getOutputStream());
        in1 = new DataInputStream(new BufferedInputStream(clientSocket1.getInputStream()));
        out1.writeByte(Constants.WAITFORPLAYER);
        clientSocket2 = serverSocket.accept();
        out2 = new DataOutputStream(clientSocket2.getOutputStream());
        in2 = new DataInputStream(new BufferedInputStream(clientSocket2.getInputStream()));

        out1.writeByte(Constants.BEGINGAME);
        out2.writeByte(Constants.BEGINGAME);

        Player p1 = new Player(state.O, out1, in1);
        Player p2 = new Player(state.O, out2, in2);
        gameloop(p1,p2);

        close();
    }


    private void close() throws IOException {
        clientSocket1.close();
        clientSocket2.close();
        serverSocket.close();
    }


    public static void main(String[] args) {
        int port = 8080;
        if(args.length == 2){
            int newport;
            try{
                newport = Integer.parseInt(args[1]);
                if(newport > 1000 && newport < 60000){
                    port = newport;
                }
            } catch(Exception e){
                System.out.print(e.getMessage());
            }

        }
        Server server=new Server();
        try{
            server.start(port);
        } catch(Exception exception){
            System.out.println(exception.getMessage());
        }
        
    }
}