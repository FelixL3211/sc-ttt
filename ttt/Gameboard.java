package ttt;

public class Gameboard {
    private state[][] board; 
    private int player1Pieces;
    private int player2Pieces;

    enum state {Blank, X, O};

    public Gameboard(){
        board = new state[3][3];
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                board[i][j] = state.Blank;
            }
        }
        player1Pieces = 0;
        player2Pieces = 0;
    }

    public String placePiece(int pos, Player player){
        int r = pos / 3;
        int c = pos % 3;
        if(pos > 8 || pos < 0){
            return "invalid argument";
        }
        if(board[r][c] != state.Blank){
            return "position taken";
        }
        if(player.nrOfPieces > 2){
            return "too many pieces currently";
        }
        board[r][c] = player.piece;
        player.nrOfPieces++;
        return "";
    }

    public String removePiece(int pos, Player player){
        if(pos > 8 || pos < 0 ){
            return "invalid argument";
        }
        if(player.nrOfPieces < 3){
            return "too few pieces currently";
        } 
        if(board[pos / 3][pos % 3] == player.piece){
            board[pos / 3][pos % 3] = state.Blank;
        } else {
            return "not your piece";
        }
        player.nrOfPieces--;
        return "";
    }

    public boolean checkWin(int pos, Player player){
        int r = pos / 3;
        int c = pos % 3;
        for(int i = 0; i < 3; i++){
            if(board[r][i] != player.piece){
                break;
            }
            if(i == 2){
                return true;
            }
        }
        for(int i = 0; i < 3; i++){
            if(board[i][c] != player.piece){
                break;
            }
            if(i == 2){
                return true;
            }
        }
        if(pos == 3 || pos == 7){
            for(int i = 0; i < 3; i++){
                if(board[2-i][i] != player.piece){
                    break;
                }
                if(i == 2){
                    return true;
                }
            }
        }
        if(pos == 0 || pos == 8){
            for(int i = 0; i < 3; i++){
                if(board[i][i] != player.piece){
                    break;
                }
                if(i == 2){
                    return true;
                }
            }
        }

        return false;
    }
}
