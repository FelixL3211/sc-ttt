package ttt;
public class Constants {
    public static final byte WAITFORPLAYER = 1;
    public static final byte BEGINGAME = 2;
    public static final byte YOUSTART = 3;
    public static final byte OPPSTART = 4;
    public static final byte YOUWON = 5;
    public static final byte OPPWON = 6;
    public static final byte PLACEPIECE = 7;
    public static final byte RMPIECE = 8;
    public static final byte OPPPLACED = 9;
    public static final byte OPPREMOED = 10;
}
