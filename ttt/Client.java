package ttt;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
    private Socket clientSocket;
    private DataInputStream in;
    private DataOutputStream out;

    public void start(int port) throws UnknownHostException, IOException{
        clientSocket = new Socket("3.65.173.2", port);
        out = new DataOutputStream(clientSocket.getOutputStream());
        in = new DataInputStream(new BufferedInputStream(clientSocket.getInputStream()));
        
        while(true){
            byte msg = in.readByte();
            System.out.println(convertMSG(msg));
            if(msg == 2){
                break;
            }
        }
    }


    public static void main(String[] args){
        int port = 8080;
        if(args.length == 2){
            int newport;
            try{
                newport = Integer.parseInt(args[1]);
                if(newport > 1000 && newport < 60000){
                    port = newport;
                }
            } catch(Exception e){
                System.out.print(e.getMessage());
            }

        }
        Client client = new Client();
        try {
            client.start(port);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private String convertMSG(byte code){
        switch(code){
            case 1:
                return "Waiting for another player to join";
            case 2: 
                return "Both players joined, lets begin";
            default:
                return "Unknow message";

        }
    }
}
